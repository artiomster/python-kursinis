import kivy
from kivy.app import App
from kivy.properties import StringProperty
from kivy.animation import Animation
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.popup import Popup
from kivy.lang import Builder
from kivy.config import Config
from Equations import *
import numpy


kivy.require('2.2.0')

global sm
# symbol √ -> \u221A


class MainScreen(Screen):
    name = "MainScreen"
    pass


class DifficultyPopupClass(Popup):
    level, NofN = 1, 1

    def swith_to_random(self):
        sm.switch_to(EquationChal(self.level, self.NofN))

    def increase_level(self):
        self.level = numpy.clip(1, int(self.ids.DifLvl.text) + 1, 3)
        self.upd_level_pr()
        return str(self.level)

    def decrease_level(self):
        self.level = numpy.clip(1, int(self.ids.DifLvl.text) - 1, 3)
        self.upd_level_pr()
        return str(self.level)

    def upd_level_pr(self):
        if self.level == 1:
            self.ids.DifLvlExt.text = "+,-"
        elif self.level == 2:
            self.ids.DifLvlExt.text = "+,-,*,/"
        elif self.level == 3:
            self.ids.DifLvlExt.text = "+,-,*,/,√,**"

    def increase_amount_of_numbers(self):
        self.NofN = int(self.ids.NofN.text) + 1
        return str(self.NofN)

    def decrease_amount_of_numbers(self):
        self.NofN = max(1, int(self.ids.NofN.text) - 1)
        return str(self.NofN)


class EquationChal(Screen):
    name = "EquationChal"
    equation_text = StringProperty()
    level = 1
    numbers_ammount = 2
    scorre_to_add = 0

    def __init__(self, level, numbers_ammount: int = 1, **kwargs):
        super().__init__(**kwargs)
        self.numbers_ammount = numbers_ammount
        self.level = level
        self.equation = self.get_equation()
        self.equation_text = str(self.equation)
        self.scorre_to_add = self.equation.score
        print(f'{self.equation_text} | {level} | {numbers_ammount}')

    def check(self, box_layout, answer: int):
        if self.equation.check(answer):
            box_layout.color = (0, 0.7, 0, 1)
        else:
            box_layout.color = (1, 0, 0, 1)
            self.fadecolor(box_layout)

    def get_equation(self):
        if self.level == 1 or self.level == 2:
            return SimpleTypeEquation(self.numbers_ammount, self.level)
        else:
            return SecondTypeEquation(self.numbers_ammount)

    def recreate(self):
        self.equation = SimpleTypeEquation(self.numbers_ammount)
        self.equation_text = str(self.equation)

    @staticmethod
    def fadecolor(widget):
        start_x = widget.x
        shake = Animation(x=start_x + 1, duration=0.02) \
              + Animation(x=start_x - 2, duration=0.02) \
              + Animation(x=start_x + 2, duration=0.02) \
              + Animation(x=start_x - 2, duration=0.02) \
              + Animation(x=start_x + 2, duration=0.02) \
              + Animation(x=start_x - 2, duration=0.02) \
              + Animation(x=start_x + 1, duration=0.02)
        anim = Animation(color=(0, 0, 0, 1)) & shake
        anim.start(widget)


class Statistics(Screen):
    name = "Statistics"
    pass


class Calculator(Screen):
    name = "Calculator"

    def check(self, textas):
        try:
            answer = eval(textas)
            self.ids.answer.text = str(answer)
            return str(answer)
        except SyntaxError:
            pass
            # self.ids.answer.text = str(answer)


class CalcChallengeApp(App):
    score = 0

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        global sm
        Builder.load_file('Stats.kv')
        Builder.load_file('Calc.kv')
        sm = ScreenManager()
        # Config.read('save.ini')

    def build(self):
        sm.switch_to(MainScreen())
        return sm

    @staticmethod
    def return_to_main():
        sm.switch_to(MainScreen())

    @staticmethod
    def swith_to_calc():
        sm.switch_to(Calculator())

    @staticmethod
    def swith_to_stat():
        sm.switch_to(Statistics())


if __name__ == "__main__":
    CalcChallengeApp().run()
