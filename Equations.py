import random
from sympy import *


class EquationBase:
    equation_text = ""
    equallyti_pos = 0
    rezultatas = [1.1]
    selected = 0
    score = 0

    def get_sympy_eq(self):
        lhs, rhs = self.equation_text.split('=')
        lhs_expr = sympify(lhs)
        rhs_expr = sympify(rhs)
        equation = Eq(lhs_expr, rhs_expr)
        answer = solve(equation)
        return equation, answer

    def check(self, answer):
        if isinstance(answer, str) and answer.lstrip('+-').replace('.', '', 1).isdigit():
            answer = float(answer)
            print(f"{self.rezultatas[0]} == {answer}")
        return self.rezultatas[0] == answer

    def __str__(self):
        return self.equation_text


class SimpleTypeEquation(EquationBase):
    def __init__(self, ammount_of_numbers: int, level: int = 1, min_n: int = 0, max_n: int = 100, is_whole_n: bool = True):
        self.ammount_of_numbers = ammount_of_numbers
        self.level = level
        if ammount_of_numbers == 0:
            self.equation, self.rezultatas = 0, [0]
            return
        if is_whole_n:
            while not (self.rezultatas[0] * 10) % 10 == 0:
                self.create(min_n, max_n)
                self.equation, self.rezultatas = self.get_sympy_eq()
        else:
            self.create(min_n, max_n)
            self.equation, self.rezultatas = self.get_sympy_eq()

    def get_symbol(self, i):
        if i == self.equallyti_pos:
            return "="
        else:
            if self.level == 1:
                _symbols = ["+", "-"]
            elif self.level == 2:
                _symbols = ["+", "-", "*", "/"]
            else:
                _symbols = ["+"]
            return random.choice(_symbols)

    def get_number(self, i, r_from, r_to):
        if i == self.selected:
            return "x"
        else:
            return random.randint(r_from, r_to)

    def create(self, r_from, r_to):
        self.selected = random.randint(0, self.ammount_of_numbers)
        self.equallyti_pos = random.randint(1, self.ammount_of_numbers)
        self.equation_text = str(self.get_number(0, r_from, r_to))
        for i in range(self.ammount_of_numbers):
            number = self.get_number(i+1, r_from, r_to)
            self.score += abs(int(number))
            symbol = self.get_symbol(i+1)
            self.equation_text += f" {symbol} {number}"


class SecondTypeEquation(EquationBase):
    def __init__(self, ammount_of_numbers: int, min_number: int = 0, max_number: int = 100):
        self.ammount_of_numbers = ammount_of_numbers
        if ammount_of_numbers != 0:
            self.create(min_number, max_number)

    def create(self, r_from, r_to):
        pass
